# Generated from main/bkit/parser/BKIT.g4 by ANTLR 4.8
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .BKITParser import BKITParser
else:
    from BKITParser import BKITParser

# This class defines a complete generic visitor for a parse tree produced by BKITParser.

class BKITVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by BKITParser#program.
    def visitProgram(self, ctx:BKITParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#declaration.
    def visitDeclaration(self, ctx:BKITParser.DeclarationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#varDec.
    def visitVarDec(self, ctx:BKITParser.VarDecContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#funcDec.
    def visitFuncDec(self, ctx:BKITParser.FuncDecContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#id_list.
    def visitId_list(self, ctx:BKITParser.Id_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#id_form.
    def visitId_form(self, ctx:BKITParser.Id_formContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#id_init.
    def visitId_init(self, ctx:BKITParser.Id_initContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#id_arr.
    def visitId_arr(self, ctx:BKITParser.Id_arrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#array_type.
    def visitArray_type(self, ctx:BKITParser.Array_typeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#element_lit.
    def visitElement_lit(self, ctx:BKITParser.Element_litContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#element_com.
    def visitElement_com(self, ctx:BKITParser.Element_comContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#subArr.
    def visitSubArr(self, ctx:BKITParser.SubArrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#expr.
    def visitExpr(self, ctx:BKITParser.ExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#expr1.
    def visitExpr1(self, ctx:BKITParser.Expr1Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#expr2.
    def visitExpr2(self, ctx:BKITParser.Expr2Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#expr3.
    def visitExpr3(self, ctx:BKITParser.Expr3Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#expr4.
    def visitExpr4(self, ctx:BKITParser.Expr4Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#expr5.
    def visitExpr5(self, ctx:BKITParser.Expr5Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#expr6.
    def visitExpr6(self, ctx:BKITParser.Expr6Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#expr7.
    def visitExpr7(self, ctx:BKITParser.Expr7Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#index_op.
    def visitIndex_op(self, ctx:BKITParser.Index_opContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#operand.
    def visitOperand(self, ctx:BKITParser.OperandContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#paralist.
    def visitParalist(self, ctx:BKITParser.ParalistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#para.
    def visitPara(self, ctx:BKITParser.ParaContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#bodypart.
    def visitBodypart(self, ctx:BKITParser.BodypartContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#statement.
    def visitStatement(self, ctx:BKITParser.StatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#assign.
    def visitAssign(self, ctx:BKITParser.AssignContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#assign_expr.
    def visitAssign_expr(self, ctx:BKITParser.Assign_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#if_stat.
    def visitIf_stat(self, ctx:BKITParser.If_statContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#for_stat.
    def visitFor_stat(self, ctx:BKITParser.For_statContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#while_stat.
    def visitWhile_stat(self, ctx:BKITParser.While_statContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#do_while.
    def visitDo_while(self, ctx:BKITParser.Do_whileContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#break_stat.
    def visitBreak_stat(self, ctx:BKITParser.Break_statContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#cont_stat.
    def visitCont_stat(self, ctx:BKITParser.Cont_statContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#call.
    def visitCall(self, ctx:BKITParser.CallContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#call_expr.
    def visitCall_expr(self, ctx:BKITParser.Call_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#ret_stat.
    def visitRet_stat(self, ctx:BKITParser.Ret_statContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#prim_datatype.
    def visitPrim_datatype(self, ctx:BKITParser.Prim_datatypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#int_lit.
    def visitInt_lit(self, ctx:BKITParser.Int_litContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#float_lit.
    def visitFloat_lit(self, ctx:BKITParser.Float_litContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#string_lit.
    def visitString_lit(self, ctx:BKITParser.String_litContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#bool_lit.
    def visitBool_lit(self, ctx:BKITParser.Bool_litContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#ident.
    def visitIdent(self, ctx:BKITParser.IdentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#mul_div.
    def visitMul_div(self, ctx:BKITParser.Mul_divContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#add_sub.
    def visitAdd_sub(self, ctx:BKITParser.Add_subContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#minus.
    def visitMinus(self, ctx:BKITParser.MinusContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#equal.
    def visitEqual(self, ctx:BKITParser.EqualContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#noteq.
    def visitNoteq(self, ctx:BKITParser.NoteqContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#great.
    def visitGreat(self, ctx:BKITParser.GreatContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#less.
    def visitLess(self, ctx:BKITParser.LessContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BKITParser#rel_sign.
    def visitRel_sign(self, ctx:BKITParser.Rel_signContext):
        return self.visitChildren(ctx)



del BKITParser