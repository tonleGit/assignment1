# Generated from main/bkit/parser/BKIT.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3G")
        buf.write("\u01e2\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t")
        buf.write("&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.\t.\4")
        buf.write("/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t\64")
        buf.write("\3\2\7\2j\n\2\f\2\16\2m\13\2\3\2\3\2\3\3\3\3\5\3s\n\3")
        buf.write("\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\5\5~\n\5\3\5\3\5")
        buf.write("\3\6\3\6\3\6\7\6\u0085\n\6\f\6\16\6\u0088\13\6\3\7\3\7")
        buf.write("\5\7\u008c\n\7\3\7\5\7\u008f\n\7\3\b\3\b\3\b\3\b\3\b\5")
        buf.write("\b\u0096\n\b\3\t\3\t\5\t\u009a\n\t\3\n\3\n\3\n\5\n\u009f")
        buf.write("\n\n\3\n\3\n\3\13\3\13\3\13\7\13\u00a6\n\13\f\13\16\13")
        buf.write("\u00a9\13\13\3\f\3\f\3\f\7\f\u00ae\n\f\f\f\16\f\u00b1")
        buf.write("\13\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u00bb\n\r\3")
        buf.write("\16\3\16\3\16\3\16\3\16\5\16\u00c2\n\16\3\17\3\17\3\17")
        buf.write("\3\17\3\17\3\17\7\17\u00ca\n\17\f\17\16\17\u00cd\13\17")
        buf.write("\3\20\3\20\3\20\3\20\3\20\3\20\3\20\7\20\u00d6\n\20\f")
        buf.write("\20\16\20\u00d9\13\20\3\21\3\21\3\21\3\21\3\21\3\21\3")
        buf.write("\21\7\21\u00e2\n\21\f\21\16\21\u00e5\13\21\3\22\3\22\3")
        buf.write("\22\5\22\u00ea\n\22\3\23\3\23\3\23\3\23\5\23\u00f0\n\23")
        buf.write("\3\24\3\24\3\24\5\24\u00f5\n\24\3\24\3\24\3\24\5\24\u00fa")
        buf.write("\n\24\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u0102\n\25\3")
        buf.write("\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u010d")
        buf.write("\n\26\3\27\3\27\3\27\3\27\3\27\5\27\u0114\n\27\3\30\3")
        buf.write("\30\3\30\3\30\3\30\7\30\u011b\n\30\f\30\16\30\u011e\13")
        buf.write("\30\3\31\3\31\5\31\u0122\n\31\3\32\3\32\3\32\7\32\u0127")
        buf.write("\n\32\f\32\16\32\u012a\13\32\3\32\3\32\3\32\3\33\3\33")
        buf.write("\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\5\33\u0139\n")
        buf.write("\33\3\34\3\34\3\34\3\35\3\35\5\35\u0140\n\35\3\35\3\35")
        buf.write("\3\35\5\35\u0145\n\35\3\35\3\35\3\35\3\36\3\36\3\36\3")
        buf.write("\36\7\36\u014e\n\36\f\36\16\36\u0151\13\36\3\36\3\36\3")
        buf.write("\36\3\36\7\36\u0157\n\36\f\36\16\36\u015a\13\36\7\36\u015c")
        buf.write("\n\36\f\36\16\36\u015f\13\36\3\36\3\36\7\36\u0163\n\36")
        buf.write("\f\36\16\36\u0166\13\36\5\36\u0168\n\36\3\36\3\36\3\36")
        buf.write("\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37")
        buf.write("\3\37\7\37\u0179\n\37\f\37\16\37\u017c\13\37\3\37\3\37")
        buf.write("\3\37\3 \3 \3 \3 \5 \u0185\n \3 \3 \3 \7 \u018a\n \f ")
        buf.write("\16 \u018d\13 \3 \3 \3 \3!\3!\7!\u0194\n!\f!\16!\u0197")
        buf.write("\13!\3!\3!\3!\3!\5!\u019d\n!\3!\3!\3!\3!\3\"\3\"\3\"\3")
        buf.write("#\3#\3#\3$\3$\3$\3%\3%\3%\3%\3%\7%\u01b1\n%\f%\16%\u01b4")
        buf.write("\13%\3%\3%\3&\3&\3&\3&\3\'\3\'\3\'\5\'\u01bf\n\'\3(\3")
        buf.write("(\3)\3)\3*\3*\3+\3+\3,\3,\3-\3-\3.\3.\3.\5.\u01d0\n.\3")
        buf.write("/\3/\3\60\3\60\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3\64")
        buf.write("\3\64\3\64\5\64\u01e0\n\64\3\64\2\5\34\36 \65\2\4\6\b")
        buf.write("\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668")
        buf.write(":<>@BDFHJLNPRTVXZ\\^`bdf\2\t\3\2#$\3\2\64\65\4\2\20\22")
        buf.write("\33\34\4\2\17\17\32\32\4\2\24\24\35\35\6\2\26\26\30\30")
        buf.write("\36\36  \6\2\25\25\27\27\37\37!!\2\u01ec\2k\3\2\2\2\4")
        buf.write("r\3\2\2\2\6t\3\2\2\2\by\3\2\2\2\n\u0081\3\2\2\2\f\u008b")
        buf.write("\3\2\2\2\16\u0090\3\2\2\2\20\u0097\3\2\2\2\22\u009b\3")
        buf.write("\2\2\2\24\u00a2\3\2\2\2\26\u00aa\3\2\2\2\30\u00ba\3\2")
        buf.write("\2\2\32\u00c1\3\2\2\2\34\u00c3\3\2\2\2\36\u00ce\3\2\2")
        buf.write("\2 \u00da\3\2\2\2\"\u00e9\3\2\2\2$\u00ef\3\2\2\2&\u00f9")
        buf.write("\3\2\2\2(\u0101\3\2\2\2*\u010c\3\2\2\2,\u0113\3\2\2\2")
        buf.write(".\u0115\3\2\2\2\60\u0121\3\2\2\2\62\u0123\3\2\2\2\64\u0138")
        buf.write("\3\2\2\2\66\u013a\3\2\2\28\u0144\3\2\2\2:\u0149\3\2\2")
        buf.write("\2<\u016c\3\2\2\2>\u0180\3\2\2\2@\u0191\3\2\2\2B\u01a2")
        buf.write("\3\2\2\2D\u01a5\3\2\2\2F\u01a8\3\2\2\2H\u01ab\3\2\2\2")
        buf.write("J\u01b7\3\2\2\2L\u01be\3\2\2\2N\u01c0\3\2\2\2P\u01c2\3")
        buf.write("\2\2\2R\u01c4\3\2\2\2T\u01c6\3\2\2\2V\u01c8\3\2\2\2X\u01ca")
        buf.write("\3\2\2\2Z\u01cf\3\2\2\2\\\u01d1\3\2\2\2^\u01d3\3\2\2\2")
        buf.write("`\u01d5\3\2\2\2b\u01d7\3\2\2\2d\u01d9\3\2\2\2f\u01df\3")
        buf.write("\2\2\2hj\5\4\3\2ih\3\2\2\2jm\3\2\2\2ki\3\2\2\2kl\3\2\2")
        buf.write("\2ln\3\2\2\2mk\3\2\2\2no\7\2\2\3o\3\3\2\2\2ps\5\6\4\2")
        buf.write("qs\5\b\5\2rp\3\2\2\2rq\3\2\2\2s\5\3\2\2\2tu\7%\2\2uv\7")
        buf.write("\4\2\2vw\5\n\6\2wx\7\3\2\2x\7\3\2\2\2yz\78\2\2z{\7\4\2")
        buf.write("\2{}\5V,\2|~\5.\30\2}|\3\2\2\2}~\3\2\2\2~\177\3\2\2\2")
        buf.write("\177\u0080\5\62\32\2\u0080\t\3\2\2\2\u0081\u0086\5\f\7")
        buf.write("\2\u0082\u0083\7\f\2\2\u0083\u0085\5\f\7\2\u0084\u0082")
        buf.write("\3\2\2\2\u0085\u0088\3\2\2\2\u0086\u0084\3\2\2\2\u0086")
        buf.write("\u0087\3\2\2\2\u0087\13\3\2\2\2\u0088\u0086\3\2\2\2\u0089")
        buf.write("\u008c\5V,\2\u008a\u008c\5\20\t\2\u008b\u0089\3\2\2\2")
        buf.write("\u008b\u008a\3\2\2\2\u008c\u008e\3\2\2\2\u008d\u008f\5")
        buf.write("\16\b\2\u008e\u008d\3\2\2\2\u008e\u008f\3\2\2\2\u008f")
        buf.write("\r\3\2\2\2\u0090\u0095\7\r\2\2\u0091\u0096\5L\'\2\u0092")
        buf.write("\u0096\5V,\2\u0093\u0096\5\22\n\2\u0094\u0096\5H%\2\u0095")
        buf.write("\u0091\3\2\2\2\u0095\u0092\3\2\2\2\u0095\u0093\3\2\2\2")
        buf.write("\u0095\u0094\3\2\2\2\u0096\17\3\2\2\2\u0097\u0099\5V,")
        buf.write("\2\u0098\u009a\5*\26\2\u0099\u0098\3\2\2\2\u0099\u009a")
        buf.write("\3\2\2\2\u009a\21\3\2\2\2\u009b\u009e\7\7\2\2\u009c\u009f")
        buf.write("\5\24\13\2\u009d\u009f\5\26\f\2\u009e\u009c\3\2\2\2\u009e")
        buf.write("\u009d\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0\u00a1\7\b\2\2")
        buf.write("\u00a1\23\3\2\2\2\u00a2\u00a7\5L\'\2\u00a3\u00a4\7\f\2")
        buf.write("\2\u00a4\u00a6\5L\'\2\u00a5\u00a3\3\2\2\2\u00a6\u00a9")
        buf.write("\3\2\2\2\u00a7\u00a5\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8")
        buf.write("\25\3\2\2\2\u00a9\u00a7\3\2\2\2\u00aa\u00af\5\30\r\2\u00ab")
        buf.write("\u00ac\7\f\2\2\u00ac\u00ae\5\30\r\2\u00ad\u00ab\3\2\2")
        buf.write("\2\u00ae\u00b1\3\2\2\2\u00af\u00ad\3\2\2\2\u00af\u00b0")
        buf.write("\3\2\2\2\u00b0\27\3\2\2\2\u00b1\u00af\3\2\2\2\u00b2\u00b3")
        buf.write("\7\7\2\2\u00b3\u00b4\5\26\f\2\u00b4\u00b5\7\b\2\2\u00b5")
        buf.write("\u00bb\3\2\2\2\u00b6\u00b7\7\7\2\2\u00b7\u00b8\5\24\13")
        buf.write("\2\u00b8\u00b9\7\b\2\2\u00b9\u00bb\3\2\2\2\u00ba\u00b2")
        buf.write("\3\2\2\2\u00ba\u00b6\3\2\2\2\u00bb\31\3\2\2\2\u00bc\u00bd")
        buf.write("\5\34\17\2\u00bd\u00be\5f\64\2\u00be\u00bf\5\34\17\2\u00bf")
        buf.write("\u00c2\3\2\2\2\u00c0\u00c2\5\34\17\2\u00c1\u00bc\3\2\2")
        buf.write("\2\u00c1\u00c0\3\2\2\2\u00c2\33\3\2\2\2\u00c3\u00c4\b")
        buf.write("\17\1\2\u00c4\u00c5\5\36\20\2\u00c5\u00cb\3\2\2\2\u00c6")
        buf.write("\u00c7\f\4\2\2\u00c7\u00c8\t\2\2\2\u00c8\u00ca\5\36\20")
        buf.write("\2\u00c9\u00c6\3\2\2\2\u00ca\u00cd\3\2\2\2\u00cb\u00c9")
        buf.write("\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\35\3\2\2\2\u00cd\u00cb")
        buf.write("\3\2\2\2\u00ce\u00cf\b\20\1\2\u00cf\u00d0\5 \21\2\u00d0")
        buf.write("\u00d7\3\2\2\2\u00d1\u00d2\f\4\2\2\u00d2\u00d3\5Z.\2\u00d3")
        buf.write("\u00d4\5 \21\2\u00d4\u00d6\3\2\2\2\u00d5\u00d1\3\2\2\2")
        buf.write("\u00d6\u00d9\3\2\2\2\u00d7\u00d5\3\2\2\2\u00d7\u00d8\3")
        buf.write("\2\2\2\u00d8\37\3\2\2\2\u00d9\u00d7\3\2\2\2\u00da\u00db")
        buf.write("\b\21\1\2\u00db\u00dc\5\"\22\2\u00dc\u00e3\3\2\2\2\u00dd")
        buf.write("\u00de\f\4\2\2\u00de\u00df\5X-\2\u00df\u00e0\5\"\22\2")
        buf.write("\u00e0\u00e2\3\2\2\2\u00e1\u00dd\3\2\2\2\u00e2\u00e5\3")
        buf.write("\2\2\2\u00e3\u00e1\3\2\2\2\u00e3\u00e4\3\2\2\2\u00e4!")
        buf.write("\3\2\2\2\u00e5\u00e3\3\2\2\2\u00e6\u00e7\7\"\2\2\u00e7")
        buf.write("\u00ea\5\"\22\2\u00e8\u00ea\5$\23\2\u00e9\u00e6\3\2\2")
        buf.write("\2\u00e9\u00e8\3\2\2\2\u00ea#\3\2\2\2\u00eb\u00ec\5\\")
        buf.write("/\2\u00ec\u00ed\5$\23\2\u00ed\u00f0\3\2\2\2\u00ee\u00f0")
        buf.write("\5&\24\2\u00ef\u00eb\3\2\2\2\u00ef\u00ee\3\2\2\2\u00f0")
        buf.write("%\3\2\2\2\u00f1\u00f5\5H%\2\u00f2\u00f5\5V,\2\u00f3\u00f5")
        buf.write("\5\20\t\2\u00f4\u00f1\3\2\2\2\u00f4\u00f2\3\2\2\2\u00f4")
        buf.write("\u00f3\3\2\2\2\u00f5\u00f6\3\2\2\2\u00f6\u00f7\5*\26\2")
        buf.write("\u00f7\u00fa\3\2\2\2\u00f8\u00fa\5(\25\2\u00f9\u00f4\3")
        buf.write("\2\2\2\u00f9\u00f8\3\2\2\2\u00fa\'\3\2\2\2\u00fb\u00fc")
        buf.write("\7\5\2\2\u00fc\u00fd\5\32\16\2\u00fd\u00fe\7\6\2\2\u00fe")
        buf.write("\u0102\3\2\2\2\u00ff\u0102\5H%\2\u0100\u0102\5,\27\2\u0101")
        buf.write("\u00fb\3\2\2\2\u0101\u00ff\3\2\2\2\u0101\u0100\3\2\2\2")
        buf.write("\u0102)\3\2\2\2\u0103\u0104\7\t\2\2\u0104\u0105\5\32\16")
        buf.write("\2\u0105\u0106\7\n\2\2\u0106\u010d\3\2\2\2\u0107\u0108")
        buf.write("\7\t\2\2\u0108\u0109\5\32\16\2\u0109\u010a\7\n\2\2\u010a")
        buf.write("\u010b\5*\26\2\u010b\u010d\3\2\2\2\u010c\u0103\3\2\2\2")
        buf.write("\u010c\u0107\3\2\2\2\u010d+\3\2\2\2\u010e\u0114\5N(\2")
        buf.write("\u010f\u0114\5P)\2\u0110\u0114\5T+\2\u0111\u0114\5V,\2")
        buf.write("\u0112\u0114\5\22\n\2\u0113\u010e\3\2\2\2\u0113\u010f")
        buf.write("\3\2\2\2\u0113\u0110\3\2\2\2\u0113\u0111\3\2\2\2\u0113")
        buf.write("\u0112\3\2\2\2\u0114-\3\2\2\2\u0115\u0116\7.\2\2\u0116")
        buf.write("\u0117\7\4\2\2\u0117\u011c\5\60\31\2\u0118\u0119\7\f\2")
        buf.write("\2\u0119\u011b\5\60\31\2\u011a\u0118\3\2\2\2\u011b\u011e")
        buf.write("\3\2\2\2\u011c\u011a\3\2\2\2\u011c\u011d\3\2\2\2\u011d")
        buf.write("/\3\2\2\2\u011e\u011c\3\2\2\2\u011f\u0122\5V,\2\u0120")
        buf.write("\u0122\5\20\t\2\u0121\u011f\3\2\2\2\u0121\u0120\3\2\2")
        buf.write("\2\u0122\61\3\2\2\2\u0123\u0124\7&\2\2\u0124\u0128\7\4")
        buf.write("\2\2\u0125\u0127\5\64\33\2\u0126\u0125\3\2\2\2\u0127\u012a")
        buf.write("\3\2\2\2\u0128\u0126\3\2\2\2\u0128\u0129\3\2\2\2\u0129")
        buf.write("\u012b\3\2\2\2\u012a\u0128\3\2\2\2\u012b\u012c\7\61\2")
        buf.write("\2\u012c\u012d\7\13\2\2\u012d\63\3\2\2\2\u012e\u0139\5")
        buf.write("\6\4\2\u012f\u0139\5\66\34\2\u0130\u0139\5:\36\2\u0131")
        buf.write("\u0139\5<\37\2\u0132\u0139\5> \2\u0133\u0139\5@!\2\u0134")
        buf.write("\u0139\5B\"\2\u0135\u0139\5D#\2\u0136\u0139\5F$\2\u0137")
        buf.write("\u0139\5J&\2\u0138\u012e\3\2\2\2\u0138\u012f\3\2\2\2\u0138")
        buf.write("\u0130\3\2\2\2\u0138\u0131\3\2\2\2\u0138\u0132\3\2\2\2")
        buf.write("\u0138\u0133\3\2\2\2\u0138\u0134\3\2\2\2\u0138\u0135\3")
        buf.write("\2\2\2\u0138\u0136\3\2\2\2\u0138\u0137\3\2\2\2\u0139\65")
        buf.write("\3\2\2\2\u013a\u013b\58\35\2\u013b\u013c\7\3\2\2\u013c")
        buf.write("\67\3\2\2\2\u013d\u013f\5V,\2\u013e\u0140\5*\26\2\u013f")
        buf.write("\u013e\3\2\2\2\u013f\u0140\3\2\2\2\u0140\u0145\3\2\2\2")
        buf.write("\u0141\u0142\5H%\2\u0142\u0143\5*\26\2\u0143\u0145\3\2")
        buf.write("\2\2\u0144\u013d\3\2\2\2\u0144\u0141\3\2\2\2\u0145\u0146")
        buf.write("\3\2\2\2\u0146\u0147\7\r\2\2\u0147\u0148\5\32\16\2\u0148")
        buf.write("9\3\2\2\2\u0149\u014a\7)\2\2\u014a\u014b\5\32\16\2\u014b")
        buf.write("\u014f\79\2\2\u014c\u014e\5\64\33\2\u014d\u014c\3\2\2")
        buf.write("\2\u014e\u0151\3\2\2\2\u014f\u014d\3\2\2\2\u014f\u0150")
        buf.write("\3\2\2\2\u0150\u015d\3\2\2\2\u0151\u014f\3\2\2\2\u0152")
        buf.write("\u0153\7,\2\2\u0153\u0154\5\32\16\2\u0154\u0158\79\2\2")
        buf.write("\u0155\u0157\5\64\33\2\u0156\u0155\3\2\2\2\u0157\u015a")
        buf.write("\3\2\2\2\u0158\u0156\3\2\2\2\u0158\u0159\3\2\2\2\u0159")
        buf.write("\u015c\3\2\2\2\u015a\u0158\3\2\2\2\u015b\u0152\3\2\2\2")
        buf.write("\u015c\u015f\3\2\2\2\u015d\u015b\3\2\2\2\u015d\u015e\3")
        buf.write("\2\2\2\u015e\u0167\3\2\2\2\u015f\u015d\3\2\2\2\u0160\u0164")
        buf.write("\7\'\2\2\u0161\u0163\5\64\33\2\u0162\u0161\3\2\2\2\u0163")
        buf.write("\u0166\3\2\2\2\u0164\u0162\3\2\2\2\u0164\u0165\3\2\2\2")
        buf.write("\u0165\u0168\3\2\2\2\u0166\u0164\3\2\2\2\u0167\u0160\3")
        buf.write("\2\2\2\u0167\u0168\3\2\2\2\u0168\u0169\3\2\2\2\u0169\u016a")
        buf.write("\7\67\2\2\u016a\u016b\7\13\2\2\u016b;\3\2\2\2\u016c\u016d")
        buf.write("\7\62\2\2\u016d\u016e\7\5\2\2\u016e\u016f\5V,\2\u016f")
        buf.write("\u0170\7\r\2\2\u0170\u0171\5\32\16\2\u0171\u0172\7\f\2")
        buf.write("\2\u0172\u0173\5\32\16\2\u0173\u0174\7\f\2\2\u0174\u0175")
        buf.write("\58\35\2\u0175\u0176\7\6\2\2\u0176\u017a\7\66\2\2\u0177")
        buf.write("\u0179\5\64\33\2\u0178\u0177\3\2\2\2\u0179\u017c\3\2\2")
        buf.write("\2\u017a\u0178\3\2\2\2\u017a\u017b\3\2\2\2\u017b\u017d")
        buf.write("\3\2\2\2\u017c\u017a\3\2\2\2\u017d\u017e\7(\2\2\u017e")
        buf.write("\u017f\7\13\2\2\u017f=\3\2\2\2\u0180\u0181\7/\2\2\u0181")
        buf.write("\u0184\7\5\2\2\u0182\u0185\5\32\16\2\u0183\u0185\5T+\2")
        buf.write("\u0184\u0182\3\2\2\2\u0184\u0183\3\2\2\2\u0185\u0186\3")
        buf.write("\2\2\2\u0186\u0187\7\6\2\2\u0187\u018b\7\66\2\2\u0188")
        buf.write("\u018a\5\64\33\2\u0189\u0188\3\2\2\2\u018a\u018d\3\2\2")
        buf.write("\2\u018b\u0189\3\2\2\2\u018b\u018c\3\2\2\2\u018c\u018e")
        buf.write("\3\2\2\2\u018d\u018b\3\2\2\2\u018e\u018f\7-\2\2\u018f")
        buf.write("\u0190\7\13\2\2\u0190?\3\2\2\2\u0191\u0195\7\66\2\2\u0192")
        buf.write("\u0194\5\64\33\2\u0193\u0192\3\2\2\2\u0194\u0197\3\2\2")
        buf.write("\2\u0195\u0193\3\2\2\2\u0195\u0196\3\2\2\2\u0196\u0198")
        buf.write("\3\2\2\2\u0197\u0195\3\2\2\2\u0198\u0199\7/\2\2\u0199")
        buf.write("\u019c\7\5\2\2\u019a\u019d\5\32\16\2\u019b\u019d\5T+\2")
        buf.write("\u019c\u019a\3\2\2\2\u019c\u019b\3\2\2\2\u019d\u019e\3")
        buf.write("\2\2\2\u019e\u019f\7\6\2\2\u019f\u01a0\7*\2\2\u01a0\u01a1")
        buf.write("\7\13\2\2\u01a1A\3\2\2\2\u01a2\u01a3\7+\2\2\u01a3\u01a4")
        buf.write("\7\3\2\2\u01a4C\3\2\2\2\u01a5\u01a6\7\60\2\2\u01a6\u01a7")
        buf.write("\7\3\2\2\u01a7E\3\2\2\2\u01a8\u01a9\5H%\2\u01a9\u01aa")
        buf.write("\7\3\2\2\u01aaG\3\2\2\2\u01ab\u01ac\5V,\2\u01ac\u01ad")
        buf.write("\7\5\2\2\u01ad\u01b2\5\32\16\2\u01ae\u01af\7\f\2\2\u01af")
        buf.write("\u01b1\5\32\16\2\u01b0\u01ae\3\2\2\2\u01b1\u01b4\3\2\2")
        buf.write("\2\u01b2\u01b0\3\2\2\2\u01b2\u01b3\3\2\2\2\u01b3\u01b5")
        buf.write("\3\2\2\2\u01b4\u01b2\3\2\2\2\u01b5\u01b6\7\6\2\2\u01b6")
        buf.write("I\3\2\2\2\u01b7\u01b8\7\63\2\2\u01b8\u01b9\5\32\16\2\u01b9")
        buf.write("\u01ba\7\3\2\2\u01baK\3\2\2\2\u01bb\u01bf\5N(\2\u01bc")
        buf.write("\u01bf\5P)\2\u01bd\u01bf\5R*\2\u01be\u01bb\3\2\2\2\u01be")
        buf.write("\u01bc\3\2\2\2\u01be\u01bd\3\2\2\2\u01bfM\3\2\2\2\u01c0")
        buf.write("\u01c1\7:\2\2\u01c1O\3\2\2\2\u01c2\u01c3\7@\2\2\u01c3")
        buf.write("Q\3\2\2\2\u01c4\u01c5\7=\2\2\u01c5S\3\2\2\2\u01c6\u01c7")
        buf.write("\t\3\2\2\u01c7U\3\2\2\2\u01c8\u01c9\7A\2\2\u01c9W\3\2")
        buf.write("\2\2\u01ca\u01cb\t\4\2\2\u01cbY\3\2\2\2\u01cc\u01d0\7")
        buf.write("\16\2\2\u01cd\u01d0\7\31\2\2\u01ce\u01d0\5\\/\2\u01cf")
        buf.write("\u01cc\3\2\2\2\u01cf\u01cd\3\2\2\2\u01cf\u01ce\3\2\2\2")
        buf.write("\u01d0[\3\2\2\2\u01d1\u01d2\t\5\2\2\u01d2]\3\2\2\2\u01d3")
        buf.write("\u01d4\7\23\2\2\u01d4_\3\2\2\2\u01d5\u01d6\t\6\2\2\u01d6")
        buf.write("a\3\2\2\2\u01d7\u01d8\t\7\2\2\u01d8c\3\2\2\2\u01d9\u01da")
        buf.write("\t\b\2\2\u01dae\3\2\2\2\u01db\u01e0\5^\60\2\u01dc\u01e0")
        buf.write("\5`\61\2\u01dd\u01e0\5b\62\2\u01de\u01e0\5d\63\2\u01df")
        buf.write("\u01db\3\2\2\2\u01df\u01dc\3\2\2\2\u01df\u01dd\3\2\2\2")
        buf.write("\u01df\u01de\3\2\2\2\u01e0g\3\2\2\2-kr}\u0086\u008b\u008e")
        buf.write("\u0095\u0099\u009e\u00a7\u00af\u00ba\u00c1\u00cb\u00d7")
        buf.write("\u00e3\u00e9\u00ef\u00f4\u00f9\u0101\u010c\u0113\u011c")
        buf.write("\u0121\u0128\u0138\u013f\u0144\u014f\u0158\u015d\u0164")
        buf.write("\u0167\u017a\u0184\u018b\u0195\u019c\u01b2\u01be\u01cf")
        buf.write("\u01df")
        return buf.getvalue()


class BKITParser ( Parser ):

    grammarFileName = "BKIT.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "';'", "':'", "'('", "')'", "'{'", "'}'", 
                     "'['", "']'", "'.'", "','", "'='", "'+'", "'-'", "'*'", 
                     "'%'", "'\\'", "'=='", "'!='", "'<='", "'>='", "'<'", 
                     "'>'", "'+.'", "'-.'", "'*.'", "'\\.'", "'=/='", "'>.'", 
                     "'<.'", "'>=.'", "'<=.'", "'!'", "'&&'", "'||'", "'Var'", 
                     "'Body'", "'Else'", "'EndFor'", "'If'", "'EndDo'", 
                     "'Break'", "'ElseIf'", "'EndWhile'", "'Parameter'", 
                     "'While'", "'Continue'", "'EndBody'", "'For'", "'Return'", 
                     "'True'", "'False'", "'Do'", "'EndIf'", "'Function'", 
                     "'Then'", "<INVALID>", "<INVALID>", "''\"'" ]

    symbolicNames = [ "<INVALID>", "SEMI", "COLON", "LP", "RP", "LB", "RB", 
                      "LSQ", "RSQ", "DOT", "COMMA", "ASS", "INT_ADD", "INT_MINUS", 
                      "INT_MUL", "MOD", "INT_DIV", "INT_EQ", "INT_NEQ", 
                      "INT_LESS_EQ", "INT_GREAT_EQ", "INT_LESS", "INT_GREAT", 
                      "FLOAT_ADD", "FLOAT_MINUS", "FLOAT_MUL", "FLOAT_DIV", 
                      "FLOAT_NEQ", "FLOAT_GREAT", "FLOAT_LESS", "FLOAT_GREAT_EQ", 
                      "FLOAT_LESS_EQ", "NOT", "AND_LOGIC", "OR_LOGIC", "VAR", 
                      "BODY", "ELSE", "ENDFOR", "IF", "ENDDO", "BREAK", 
                      "ELSEIF", "ENDWHILE", "PARA", "WHILE", "CONITNUE", 
                      "ENDBODY", "FOR", "RETURN", "TRUE", "FALSE", "DO", 
                      "ENDIF", "FUNCTION", "THEN", "INTLIT", "ESC_CHAR", 
                      "SPEC_ESC", "STRINGLIT", "HEX", "OCTAL", "FLOATLIT", 
                      "ID", "WS", "BLOCKCMT", "ERROR_CHAR", "UNCLOSE_STRING", 
                      "ILLEGAL_ESCAPE", "UNTERMINATED_COMMENT" ]

    RULE_program = 0
    RULE_declaration = 1
    RULE_varDec = 2
    RULE_funcDec = 3
    RULE_id_list = 4
    RULE_id_form = 5
    RULE_id_init = 6
    RULE_id_arr = 7
    RULE_array_type = 8
    RULE_element_lit = 9
    RULE_element_com = 10
    RULE_subArr = 11
    RULE_expr = 12
    RULE_expr1 = 13
    RULE_expr2 = 14
    RULE_expr3 = 15
    RULE_expr4 = 16
    RULE_expr5 = 17
    RULE_expr6 = 18
    RULE_expr7 = 19
    RULE_index_op = 20
    RULE_operand = 21
    RULE_paralist = 22
    RULE_para = 23
    RULE_bodypart = 24
    RULE_statement = 25
    RULE_assign = 26
    RULE_assign_expr = 27
    RULE_if_stat = 28
    RULE_for_stat = 29
    RULE_while_stat = 30
    RULE_do_while = 31
    RULE_break_stat = 32
    RULE_cont_stat = 33
    RULE_call = 34
    RULE_call_expr = 35
    RULE_ret_stat = 36
    RULE_prim_datatype = 37
    RULE_int_lit = 38
    RULE_float_lit = 39
    RULE_string_lit = 40
    RULE_bool_lit = 41
    RULE_ident = 42
    RULE_mul_div = 43
    RULE_add_sub = 44
    RULE_minus = 45
    RULE_equal = 46
    RULE_noteq = 47
    RULE_great = 48
    RULE_less = 49
    RULE_rel_sign = 50

    ruleNames =  [ "program", "declaration", "varDec", "funcDec", "id_list", 
                   "id_form", "id_init", "id_arr", "array_type", "element_lit", 
                   "element_com", "subArr", "expr", "expr1", "expr2", "expr3", 
                   "expr4", "expr5", "expr6", "expr7", "index_op", "operand", 
                   "paralist", "para", "bodypart", "statement", "assign", 
                   "assign_expr", "if_stat", "for_stat", "while_stat", "do_while", 
                   "break_stat", "cont_stat", "call", "call_expr", "ret_stat", 
                   "prim_datatype", "int_lit", "float_lit", "string_lit", 
                   "bool_lit", "ident", "mul_div", "add_sub", "minus", "equal", 
                   "noteq", "great", "less", "rel_sign" ]

    EOF = Token.EOF
    SEMI=1
    COLON=2
    LP=3
    RP=4
    LB=5
    RB=6
    LSQ=7
    RSQ=8
    DOT=9
    COMMA=10
    ASS=11
    INT_ADD=12
    INT_MINUS=13
    INT_MUL=14
    MOD=15
    INT_DIV=16
    INT_EQ=17
    INT_NEQ=18
    INT_LESS_EQ=19
    INT_GREAT_EQ=20
    INT_LESS=21
    INT_GREAT=22
    FLOAT_ADD=23
    FLOAT_MINUS=24
    FLOAT_MUL=25
    FLOAT_DIV=26
    FLOAT_NEQ=27
    FLOAT_GREAT=28
    FLOAT_LESS=29
    FLOAT_GREAT_EQ=30
    FLOAT_LESS_EQ=31
    NOT=32
    AND_LOGIC=33
    OR_LOGIC=34
    VAR=35
    BODY=36
    ELSE=37
    ENDFOR=38
    IF=39
    ENDDO=40
    BREAK=41
    ELSEIF=42
    ENDWHILE=43
    PARA=44
    WHILE=45
    CONITNUE=46
    ENDBODY=47
    FOR=48
    RETURN=49
    TRUE=50
    FALSE=51
    DO=52
    ENDIF=53
    FUNCTION=54
    THEN=55
    INTLIT=56
    ESC_CHAR=57
    SPEC_ESC=58
    STRINGLIT=59
    HEX=60
    OCTAL=61
    FLOATLIT=62
    ID=63
    WS=64
    BLOCKCMT=65
    ERROR_CHAR=66
    UNCLOSE_STRING=67
    ILLEGAL_ESCAPE=68
    UNTERMINATED_COMMENT=69

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(BKITParser.EOF, 0)

        def declaration(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.DeclarationContext)
            else:
                return self.getTypedRuleContext(BKITParser.DeclarationContext,i)


        def getRuleIndex(self):
            return BKITParser.RULE_program

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = BKITParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 105
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==BKITParser.VAR or _la==BKITParser.FUNCTION:
                self.state = 102
                self.declaration()
                self.state = 107
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 108
            self.match(BKITParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DeclarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varDec(self):
            return self.getTypedRuleContext(BKITParser.VarDecContext,0)


        def funcDec(self):
            return self.getTypedRuleContext(BKITParser.FuncDecContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_declaration

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDeclaration" ):
                return visitor.visitDeclaration(self)
            else:
                return visitor.visitChildren(self)




    def declaration(self):

        localctx = BKITParser.DeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_declaration)
        try:
            self.state = 112
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKITParser.VAR]:
                self.enterOuterAlt(localctx, 1)
                self.state = 110
                self.varDec()
                pass
            elif token in [BKITParser.FUNCTION]:
                self.enterOuterAlt(localctx, 2)
                self.state = 111
                self.funcDec()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarDecContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VAR(self):
            return self.getToken(BKITParser.VAR, 0)

        def COLON(self):
            return self.getToken(BKITParser.COLON, 0)

        def id_list(self):
            return self.getTypedRuleContext(BKITParser.Id_listContext,0)


        def SEMI(self):
            return self.getToken(BKITParser.SEMI, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_varDec

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVarDec" ):
                return visitor.visitVarDec(self)
            else:
                return visitor.visitChildren(self)




    def varDec(self):

        localctx = BKITParser.VarDecContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_varDec)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 114
            self.match(BKITParser.VAR)
            self.state = 115
            self.match(BKITParser.COLON)
            self.state = 116
            self.id_list()
            self.state = 117
            self.match(BKITParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FuncDecContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FUNCTION(self):
            return self.getToken(BKITParser.FUNCTION, 0)

        def COLON(self):
            return self.getToken(BKITParser.COLON, 0)

        def ident(self):
            return self.getTypedRuleContext(BKITParser.IdentContext,0)


        def bodypart(self):
            return self.getTypedRuleContext(BKITParser.BodypartContext,0)


        def paralist(self):
            return self.getTypedRuleContext(BKITParser.ParalistContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_funcDec

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncDec" ):
                return visitor.visitFuncDec(self)
            else:
                return visitor.visitChildren(self)




    def funcDec(self):

        localctx = BKITParser.FuncDecContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_funcDec)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 119
            self.match(BKITParser.FUNCTION)
            self.state = 120
            self.match(BKITParser.COLON)
            self.state = 121
            self.ident()
            self.state = 123
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BKITParser.PARA:
                self.state = 122
                self.paralist()


            self.state = 125
            self.bodypart()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Id_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def id_form(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.Id_formContext)
            else:
                return self.getTypedRuleContext(BKITParser.Id_formContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(BKITParser.COMMA)
            else:
                return self.getToken(BKITParser.COMMA, i)

        def getRuleIndex(self):
            return BKITParser.RULE_id_list

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitId_list" ):
                return visitor.visitId_list(self)
            else:
                return visitor.visitChildren(self)




    def id_list(self):

        localctx = BKITParser.Id_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_id_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 127
            self.id_form()
            self.state = 132
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==BKITParser.COMMA:
                self.state = 128
                self.match(BKITParser.COMMA)
                self.state = 129
                self.id_form()
                self.state = 134
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Id_formContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ident(self):
            return self.getTypedRuleContext(BKITParser.IdentContext,0)


        def id_arr(self):
            return self.getTypedRuleContext(BKITParser.Id_arrContext,0)


        def id_init(self):
            return self.getTypedRuleContext(BKITParser.Id_initContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_id_form

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitId_form" ):
                return visitor.visitId_form(self)
            else:
                return visitor.visitChildren(self)




    def id_form(self):

        localctx = BKITParser.Id_formContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_id_form)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 137
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.state = 135
                self.ident()
                pass

            elif la_ == 2:
                self.state = 136
                self.id_arr()
                pass


            self.state = 140
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BKITParser.ASS:
                self.state = 139
                self.id_init()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Id_initContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ASS(self):
            return self.getToken(BKITParser.ASS, 0)

        def prim_datatype(self):
            return self.getTypedRuleContext(BKITParser.Prim_datatypeContext,0)


        def ident(self):
            return self.getTypedRuleContext(BKITParser.IdentContext,0)


        def array_type(self):
            return self.getTypedRuleContext(BKITParser.Array_typeContext,0)


        def call_expr(self):
            return self.getTypedRuleContext(BKITParser.Call_exprContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_id_init

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitId_init" ):
                return visitor.visitId_init(self)
            else:
                return visitor.visitChildren(self)




    def id_init(self):

        localctx = BKITParser.Id_initContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_id_init)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 142
            self.match(BKITParser.ASS)
            self.state = 147
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
            if la_ == 1:
                self.state = 143
                self.prim_datatype()
                pass

            elif la_ == 2:
                self.state = 144
                self.ident()
                pass

            elif la_ == 3:
                self.state = 145
                self.array_type()
                pass

            elif la_ == 4:
                self.state = 146
                self.call_expr()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Id_arrContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ident(self):
            return self.getTypedRuleContext(BKITParser.IdentContext,0)


        def index_op(self):
            return self.getTypedRuleContext(BKITParser.Index_opContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_id_arr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitId_arr" ):
                return visitor.visitId_arr(self)
            else:
                return visitor.visitChildren(self)




    def id_arr(self):

        localctx = BKITParser.Id_arrContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_id_arr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 149
            self.ident()
            self.state = 151
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,7,self._ctx)
            if la_ == 1:
                self.state = 150
                self.index_op()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Array_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LB(self):
            return self.getToken(BKITParser.LB, 0)

        def RB(self):
            return self.getToken(BKITParser.RB, 0)

        def element_lit(self):
            return self.getTypedRuleContext(BKITParser.Element_litContext,0)


        def element_com(self):
            return self.getTypedRuleContext(BKITParser.Element_comContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_array_type

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArray_type" ):
                return visitor.visitArray_type(self)
            else:
                return visitor.visitChildren(self)




    def array_type(self):

        localctx = BKITParser.Array_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_array_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 153
            self.match(BKITParser.LB)
            self.state = 156
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKITParser.INTLIT, BKITParser.STRINGLIT, BKITParser.FLOATLIT]:
                self.state = 154
                self.element_lit()
                pass
            elif token in [BKITParser.LB]:
                self.state = 155
                self.element_com()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 158
            self.match(BKITParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Element_litContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def prim_datatype(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.Prim_datatypeContext)
            else:
                return self.getTypedRuleContext(BKITParser.Prim_datatypeContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(BKITParser.COMMA)
            else:
                return self.getToken(BKITParser.COMMA, i)

        def getRuleIndex(self):
            return BKITParser.RULE_element_lit

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitElement_lit" ):
                return visitor.visitElement_lit(self)
            else:
                return visitor.visitChildren(self)




    def element_lit(self):

        localctx = BKITParser.Element_litContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_element_lit)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 160
            self.prim_datatype()
            self.state = 165
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==BKITParser.COMMA:
                self.state = 161
                self.match(BKITParser.COMMA)
                self.state = 162
                self.prim_datatype()
                self.state = 167
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Element_comContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def subArr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.SubArrContext)
            else:
                return self.getTypedRuleContext(BKITParser.SubArrContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(BKITParser.COMMA)
            else:
                return self.getToken(BKITParser.COMMA, i)

        def getRuleIndex(self):
            return BKITParser.RULE_element_com

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitElement_com" ):
                return visitor.visitElement_com(self)
            else:
                return visitor.visitChildren(self)




    def element_com(self):

        localctx = BKITParser.Element_comContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_element_com)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 168
            self.subArr()
            self.state = 173
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==BKITParser.COMMA:
                self.state = 169
                self.match(BKITParser.COMMA)
                self.state = 170
                self.subArr()
                self.state = 175
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SubArrContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LB(self):
            return self.getToken(BKITParser.LB, 0)

        def element_com(self):
            return self.getTypedRuleContext(BKITParser.Element_comContext,0)


        def RB(self):
            return self.getToken(BKITParser.RB, 0)

        def element_lit(self):
            return self.getTypedRuleContext(BKITParser.Element_litContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_subArr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSubArr" ):
                return visitor.visitSubArr(self)
            else:
                return visitor.visitChildren(self)




    def subArr(self):

        localctx = BKITParser.SubArrContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_subArr)
        try:
            self.state = 184
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 176
                self.match(BKITParser.LB)
                self.state = 177
                self.element_com()
                self.state = 178
                self.match(BKITParser.RB)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 180
                self.match(BKITParser.LB)
                self.state = 181
                self.element_lit()
                self.state = 182
                self.match(BKITParser.RB)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr1(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.Expr1Context)
            else:
                return self.getTypedRuleContext(BKITParser.Expr1Context,i)


        def rel_sign(self):
            return self.getTypedRuleContext(BKITParser.Rel_signContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_expr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)




    def expr(self):

        localctx = BKITParser.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_expr)
        try:
            self.state = 191
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,12,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 186
                self.expr1(0)
                self.state = 187
                self.rel_sign()
                self.state = 188
                self.expr1(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 190
                self.expr1(0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expr1Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr2(self):
            return self.getTypedRuleContext(BKITParser.Expr2Context,0)


        def expr1(self):
            return self.getTypedRuleContext(BKITParser.Expr1Context,0)


        def AND_LOGIC(self):
            return self.getToken(BKITParser.AND_LOGIC, 0)

        def OR_LOGIC(self):
            return self.getToken(BKITParser.OR_LOGIC, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_expr1

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr1" ):
                return visitor.visitExpr1(self)
            else:
                return visitor.visitChildren(self)



    def expr1(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKITParser.Expr1Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 26
        self.enterRecursionRule(localctx, 26, self.RULE_expr1, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 194
            self.expr2(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 201
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,13,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = BKITParser.Expr1Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_expr1)
                    self.state = 196
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 197
                    _la = self._input.LA(1)
                    if not(_la==BKITParser.AND_LOGIC or _la==BKITParser.OR_LOGIC):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 198
                    self.expr2(0) 
                self.state = 203
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,13,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expr2Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr3(self):
            return self.getTypedRuleContext(BKITParser.Expr3Context,0)


        def expr2(self):
            return self.getTypedRuleContext(BKITParser.Expr2Context,0)


        def add_sub(self):
            return self.getTypedRuleContext(BKITParser.Add_subContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_expr2

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr2" ):
                return visitor.visitExpr2(self)
            else:
                return visitor.visitChildren(self)



    def expr2(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKITParser.Expr2Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 28
        self.enterRecursionRule(localctx, 28, self.RULE_expr2, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 205
            self.expr3(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 213
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,14,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = BKITParser.Expr2Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_expr2)
                    self.state = 207
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 208
                    self.add_sub()
                    self.state = 209
                    self.expr3(0) 
                self.state = 215
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,14,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expr3Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr4(self):
            return self.getTypedRuleContext(BKITParser.Expr4Context,0)


        def expr3(self):
            return self.getTypedRuleContext(BKITParser.Expr3Context,0)


        def mul_div(self):
            return self.getTypedRuleContext(BKITParser.Mul_divContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_expr3

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr3" ):
                return visitor.visitExpr3(self)
            else:
                return visitor.visitChildren(self)



    def expr3(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKITParser.Expr3Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 30
        self.enterRecursionRule(localctx, 30, self.RULE_expr3, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 217
            self.expr4()
            self._ctx.stop = self._input.LT(-1)
            self.state = 225
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,15,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = BKITParser.Expr3Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_expr3)
                    self.state = 219
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 220
                    self.mul_div()
                    self.state = 221
                    self.expr4() 
                self.state = 227
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,15,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expr4Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NOT(self):
            return self.getToken(BKITParser.NOT, 0)

        def expr4(self):
            return self.getTypedRuleContext(BKITParser.Expr4Context,0)


        def expr5(self):
            return self.getTypedRuleContext(BKITParser.Expr5Context,0)


        def getRuleIndex(self):
            return BKITParser.RULE_expr4

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr4" ):
                return visitor.visitExpr4(self)
            else:
                return visitor.visitChildren(self)




    def expr4(self):

        localctx = BKITParser.Expr4Context(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_expr4)
        try:
            self.state = 231
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKITParser.NOT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 228
                self.match(BKITParser.NOT)
                self.state = 229
                self.expr4()
                pass
            elif token in [BKITParser.LP, BKITParser.LB, BKITParser.INT_MINUS, BKITParser.FLOAT_MINUS, BKITParser.TRUE, BKITParser.FALSE, BKITParser.INTLIT, BKITParser.FLOATLIT, BKITParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 230
                self.expr5()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expr5Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def minus(self):
            return self.getTypedRuleContext(BKITParser.MinusContext,0)


        def expr5(self):
            return self.getTypedRuleContext(BKITParser.Expr5Context,0)


        def expr6(self):
            return self.getTypedRuleContext(BKITParser.Expr6Context,0)


        def getRuleIndex(self):
            return BKITParser.RULE_expr5

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr5" ):
                return visitor.visitExpr5(self)
            else:
                return visitor.visitChildren(self)




    def expr5(self):

        localctx = BKITParser.Expr5Context(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_expr5)
        try:
            self.state = 237
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKITParser.INT_MINUS, BKITParser.FLOAT_MINUS]:
                self.enterOuterAlt(localctx, 1)
                self.state = 233
                self.minus()
                self.state = 234
                self.expr5()
                pass
            elif token in [BKITParser.LP, BKITParser.LB, BKITParser.TRUE, BKITParser.FALSE, BKITParser.INTLIT, BKITParser.FLOATLIT, BKITParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 236
                self.expr6()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expr6Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def index_op(self):
            return self.getTypedRuleContext(BKITParser.Index_opContext,0)


        def call_expr(self):
            return self.getTypedRuleContext(BKITParser.Call_exprContext,0)


        def ident(self):
            return self.getTypedRuleContext(BKITParser.IdentContext,0)


        def id_arr(self):
            return self.getTypedRuleContext(BKITParser.Id_arrContext,0)


        def expr7(self):
            return self.getTypedRuleContext(BKITParser.Expr7Context,0)


        def getRuleIndex(self):
            return BKITParser.RULE_expr6

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr6" ):
                return visitor.visitExpr6(self)
            else:
                return visitor.visitChildren(self)




    def expr6(self):

        localctx = BKITParser.Expr6Context(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_expr6)
        try:
            self.state = 247
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,19,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 242
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
                if la_ == 1:
                    self.state = 239
                    self.call_expr()
                    pass

                elif la_ == 2:
                    self.state = 240
                    self.ident()
                    pass

                elif la_ == 3:
                    self.state = 241
                    self.id_arr()
                    pass


                self.state = 244
                self.index_op()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 246
                self.expr7()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expr7Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LP(self):
            return self.getToken(BKITParser.LP, 0)

        def expr(self):
            return self.getTypedRuleContext(BKITParser.ExprContext,0)


        def RP(self):
            return self.getToken(BKITParser.RP, 0)

        def call_expr(self):
            return self.getTypedRuleContext(BKITParser.Call_exprContext,0)


        def operand(self):
            return self.getTypedRuleContext(BKITParser.OperandContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_expr7

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr7" ):
                return visitor.visitExpr7(self)
            else:
                return visitor.visitChildren(self)




    def expr7(self):

        localctx = BKITParser.Expr7Context(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_expr7)
        try:
            self.state = 255
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,20,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 249
                self.match(BKITParser.LP)
                self.state = 250
                self.expr()
                self.state = 251
                self.match(BKITParser.RP)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 253
                self.call_expr()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 254
                self.operand()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Index_opContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LSQ(self):
            return self.getToken(BKITParser.LSQ, 0)

        def expr(self):
            return self.getTypedRuleContext(BKITParser.ExprContext,0)


        def RSQ(self):
            return self.getToken(BKITParser.RSQ, 0)

        def index_op(self):
            return self.getTypedRuleContext(BKITParser.Index_opContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_index_op

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIndex_op" ):
                return visitor.visitIndex_op(self)
            else:
                return visitor.visitChildren(self)




    def index_op(self):

        localctx = BKITParser.Index_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_index_op)
        try:
            self.state = 266
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,21,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 257
                self.match(BKITParser.LSQ)
                self.state = 258
                self.expr()
                self.state = 259
                self.match(BKITParser.RSQ)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 261
                self.match(BKITParser.LSQ)
                self.state = 262
                self.expr()
                self.state = 263
                self.match(BKITParser.RSQ)
                self.state = 264
                self.index_op()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class OperandContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def int_lit(self):
            return self.getTypedRuleContext(BKITParser.Int_litContext,0)


        def float_lit(self):
            return self.getTypedRuleContext(BKITParser.Float_litContext,0)


        def bool_lit(self):
            return self.getTypedRuleContext(BKITParser.Bool_litContext,0)


        def ident(self):
            return self.getTypedRuleContext(BKITParser.IdentContext,0)


        def array_type(self):
            return self.getTypedRuleContext(BKITParser.Array_typeContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_operand

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOperand" ):
                return visitor.visitOperand(self)
            else:
                return visitor.visitChildren(self)




    def operand(self):

        localctx = BKITParser.OperandContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_operand)
        try:
            self.state = 273
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKITParser.INTLIT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 268
                self.int_lit()
                pass
            elif token in [BKITParser.FLOATLIT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 269
                self.float_lit()
                pass
            elif token in [BKITParser.TRUE, BKITParser.FALSE]:
                self.enterOuterAlt(localctx, 3)
                self.state = 270
                self.bool_lit()
                pass
            elif token in [BKITParser.ID]:
                self.enterOuterAlt(localctx, 4)
                self.state = 271
                self.ident()
                pass
            elif token in [BKITParser.LB]:
                self.enterOuterAlt(localctx, 5)
                self.state = 272
                self.array_type()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParalistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def PARA(self):
            return self.getToken(BKITParser.PARA, 0)

        def COLON(self):
            return self.getToken(BKITParser.COLON, 0)

        def para(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.ParaContext)
            else:
                return self.getTypedRuleContext(BKITParser.ParaContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(BKITParser.COMMA)
            else:
                return self.getToken(BKITParser.COMMA, i)

        def getRuleIndex(self):
            return BKITParser.RULE_paralist

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParalist" ):
                return visitor.visitParalist(self)
            else:
                return visitor.visitChildren(self)




    def paralist(self):

        localctx = BKITParser.ParalistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_paralist)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 275
            self.match(BKITParser.PARA)
            self.state = 276
            self.match(BKITParser.COLON)
            self.state = 277
            self.para()
            self.state = 282
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==BKITParser.COMMA:
                self.state = 278
                self.match(BKITParser.COMMA)
                self.state = 279
                self.para()
                self.state = 284
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParaContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ident(self):
            return self.getTypedRuleContext(BKITParser.IdentContext,0)


        def id_arr(self):
            return self.getTypedRuleContext(BKITParser.Id_arrContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_para

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPara" ):
                return visitor.visitPara(self)
            else:
                return visitor.visitChildren(self)




    def para(self):

        localctx = BKITParser.ParaContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_para)
        try:
            self.state = 287
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,24,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 285
                self.ident()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 286
                self.id_arr()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BodypartContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BODY(self):
            return self.getToken(BKITParser.BODY, 0)

        def COLON(self):
            return self.getToken(BKITParser.COLON, 0)

        def ENDBODY(self):
            return self.getToken(BKITParser.ENDBODY, 0)

        def DOT(self):
            return self.getToken(BKITParser.DOT, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.StatementContext)
            else:
                return self.getTypedRuleContext(BKITParser.StatementContext,i)


        def getRuleIndex(self):
            return BKITParser.RULE_bodypart

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBodypart" ):
                return visitor.visitBodypart(self)
            else:
                return visitor.visitChildren(self)




    def bodypart(self):

        localctx = BKITParser.BodypartContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_bodypart)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 289
            self.match(BKITParser.BODY)
            self.state = 290
            self.match(BKITParser.COLON)
            self.state = 294
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKITParser.VAR) | (1 << BKITParser.IF) | (1 << BKITParser.BREAK) | (1 << BKITParser.WHILE) | (1 << BKITParser.CONITNUE) | (1 << BKITParser.FOR) | (1 << BKITParser.RETURN) | (1 << BKITParser.DO) | (1 << BKITParser.ID))) != 0):
                self.state = 291
                self.statement()
                self.state = 296
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 297
            self.match(BKITParser.ENDBODY)
            self.state = 298
            self.match(BKITParser.DOT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varDec(self):
            return self.getTypedRuleContext(BKITParser.VarDecContext,0)


        def assign(self):
            return self.getTypedRuleContext(BKITParser.AssignContext,0)


        def if_stat(self):
            return self.getTypedRuleContext(BKITParser.If_statContext,0)


        def for_stat(self):
            return self.getTypedRuleContext(BKITParser.For_statContext,0)


        def while_stat(self):
            return self.getTypedRuleContext(BKITParser.While_statContext,0)


        def do_while(self):
            return self.getTypedRuleContext(BKITParser.Do_whileContext,0)


        def break_stat(self):
            return self.getTypedRuleContext(BKITParser.Break_statContext,0)


        def cont_stat(self):
            return self.getTypedRuleContext(BKITParser.Cont_statContext,0)


        def call(self):
            return self.getTypedRuleContext(BKITParser.CallContext,0)


        def ret_stat(self):
            return self.getTypedRuleContext(BKITParser.Ret_statContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_statement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStatement" ):
                return visitor.visitStatement(self)
            else:
                return visitor.visitChildren(self)




    def statement(self):

        localctx = BKITParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_statement)
        try:
            self.state = 310
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,26,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 300
                self.varDec()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 301
                self.assign()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 302
                self.if_stat()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 303
                self.for_stat()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 304
                self.while_stat()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 305
                self.do_while()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 306
                self.break_stat()
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 307
                self.cont_stat()
                pass

            elif la_ == 9:
                self.enterOuterAlt(localctx, 9)
                self.state = 308
                self.call()
                pass

            elif la_ == 10:
                self.enterOuterAlt(localctx, 10)
                self.state = 309
                self.ret_stat()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AssignContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def assign_expr(self):
            return self.getTypedRuleContext(BKITParser.Assign_exprContext,0)


        def SEMI(self):
            return self.getToken(BKITParser.SEMI, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_assign

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssign" ):
                return visitor.visitAssign(self)
            else:
                return visitor.visitChildren(self)




    def assign(self):

        localctx = BKITParser.AssignContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_assign)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 312
            self.assign_expr()
            self.state = 313
            self.match(BKITParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Assign_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ASS(self):
            return self.getToken(BKITParser.ASS, 0)

        def expr(self):
            return self.getTypedRuleContext(BKITParser.ExprContext,0)


        def ident(self):
            return self.getTypedRuleContext(BKITParser.IdentContext,0)


        def call_expr(self):
            return self.getTypedRuleContext(BKITParser.Call_exprContext,0)


        def index_op(self):
            return self.getTypedRuleContext(BKITParser.Index_opContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_assign_expr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssign_expr" ):
                return visitor.visitAssign_expr(self)
            else:
                return visitor.visitChildren(self)




    def assign_expr(self):

        localctx = BKITParser.Assign_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_assign_expr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 322
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,28,self._ctx)
            if la_ == 1:
                self.state = 315
                self.ident()
                self.state = 317
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==BKITParser.LSQ:
                    self.state = 316
                    self.index_op()


                pass

            elif la_ == 2:
                self.state = 319
                self.call_expr()
                self.state = 320
                self.index_op()
                pass


            self.state = 324
            self.match(BKITParser.ASS)
            self.state = 325
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class If_statContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(BKITParser.IF, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.ExprContext)
            else:
                return self.getTypedRuleContext(BKITParser.ExprContext,i)


        def THEN(self, i:int=None):
            if i is None:
                return self.getTokens(BKITParser.THEN)
            else:
                return self.getToken(BKITParser.THEN, i)

        def ENDIF(self):
            return self.getToken(BKITParser.ENDIF, 0)

        def DOT(self):
            return self.getToken(BKITParser.DOT, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.StatementContext)
            else:
                return self.getTypedRuleContext(BKITParser.StatementContext,i)


        def ELSEIF(self, i:int=None):
            if i is None:
                return self.getTokens(BKITParser.ELSEIF)
            else:
                return self.getToken(BKITParser.ELSEIF, i)

        def ELSE(self):
            return self.getToken(BKITParser.ELSE, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_if_stat

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIf_stat" ):
                return visitor.visitIf_stat(self)
            else:
                return visitor.visitChildren(self)




    def if_stat(self):

        localctx = BKITParser.If_statContext(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_if_stat)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 327
            self.match(BKITParser.IF)
            self.state = 328
            self.expr()
            self.state = 329
            self.match(BKITParser.THEN)
            self.state = 333
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKITParser.VAR) | (1 << BKITParser.IF) | (1 << BKITParser.BREAK) | (1 << BKITParser.WHILE) | (1 << BKITParser.CONITNUE) | (1 << BKITParser.FOR) | (1 << BKITParser.RETURN) | (1 << BKITParser.DO) | (1 << BKITParser.ID))) != 0):
                self.state = 330
                self.statement()
                self.state = 335
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 347
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==BKITParser.ELSEIF:
                self.state = 336
                self.match(BKITParser.ELSEIF)
                self.state = 337
                self.expr()
                self.state = 338
                self.match(BKITParser.THEN)
                self.state = 342
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKITParser.VAR) | (1 << BKITParser.IF) | (1 << BKITParser.BREAK) | (1 << BKITParser.WHILE) | (1 << BKITParser.CONITNUE) | (1 << BKITParser.FOR) | (1 << BKITParser.RETURN) | (1 << BKITParser.DO) | (1 << BKITParser.ID))) != 0):
                    self.state = 339
                    self.statement()
                    self.state = 344
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 349
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 357
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BKITParser.ELSE:
                self.state = 350
                self.match(BKITParser.ELSE)
                self.state = 354
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKITParser.VAR) | (1 << BKITParser.IF) | (1 << BKITParser.BREAK) | (1 << BKITParser.WHILE) | (1 << BKITParser.CONITNUE) | (1 << BKITParser.FOR) | (1 << BKITParser.RETURN) | (1 << BKITParser.DO) | (1 << BKITParser.ID))) != 0):
                    self.state = 351
                    self.statement()
                    self.state = 356
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 359
            self.match(BKITParser.ENDIF)
            self.state = 360
            self.match(BKITParser.DOT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class For_statContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FOR(self):
            return self.getToken(BKITParser.FOR, 0)

        def LP(self):
            return self.getToken(BKITParser.LP, 0)

        def ident(self):
            return self.getTypedRuleContext(BKITParser.IdentContext,0)


        def ASS(self):
            return self.getToken(BKITParser.ASS, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.ExprContext)
            else:
                return self.getTypedRuleContext(BKITParser.ExprContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(BKITParser.COMMA)
            else:
                return self.getToken(BKITParser.COMMA, i)

        def assign_expr(self):
            return self.getTypedRuleContext(BKITParser.Assign_exprContext,0)


        def RP(self):
            return self.getToken(BKITParser.RP, 0)

        def DO(self):
            return self.getToken(BKITParser.DO, 0)

        def ENDFOR(self):
            return self.getToken(BKITParser.ENDFOR, 0)

        def DOT(self):
            return self.getToken(BKITParser.DOT, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.StatementContext)
            else:
                return self.getTypedRuleContext(BKITParser.StatementContext,i)


        def getRuleIndex(self):
            return BKITParser.RULE_for_stat

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFor_stat" ):
                return visitor.visitFor_stat(self)
            else:
                return visitor.visitChildren(self)




    def for_stat(self):

        localctx = BKITParser.For_statContext(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_for_stat)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 362
            self.match(BKITParser.FOR)
            self.state = 363
            self.match(BKITParser.LP)
            self.state = 364
            self.ident()
            self.state = 365
            self.match(BKITParser.ASS)
            self.state = 366
            self.expr()
            self.state = 367
            self.match(BKITParser.COMMA)
            self.state = 368
            self.expr()
            self.state = 369
            self.match(BKITParser.COMMA)
            self.state = 370
            self.assign_expr()
            self.state = 371
            self.match(BKITParser.RP)
            self.state = 372
            self.match(BKITParser.DO)
            self.state = 376
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKITParser.VAR) | (1 << BKITParser.IF) | (1 << BKITParser.BREAK) | (1 << BKITParser.WHILE) | (1 << BKITParser.CONITNUE) | (1 << BKITParser.FOR) | (1 << BKITParser.RETURN) | (1 << BKITParser.DO) | (1 << BKITParser.ID))) != 0):
                self.state = 373
                self.statement()
                self.state = 378
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 379
            self.match(BKITParser.ENDFOR)
            self.state = 380
            self.match(BKITParser.DOT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class While_statContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WHILE(self):
            return self.getToken(BKITParser.WHILE, 0)

        def LP(self):
            return self.getToken(BKITParser.LP, 0)

        def RP(self):
            return self.getToken(BKITParser.RP, 0)

        def DO(self):
            return self.getToken(BKITParser.DO, 0)

        def ENDWHILE(self):
            return self.getToken(BKITParser.ENDWHILE, 0)

        def DOT(self):
            return self.getToken(BKITParser.DOT, 0)

        def expr(self):
            return self.getTypedRuleContext(BKITParser.ExprContext,0)


        def bool_lit(self):
            return self.getTypedRuleContext(BKITParser.Bool_litContext,0)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.StatementContext)
            else:
                return self.getTypedRuleContext(BKITParser.StatementContext,i)


        def getRuleIndex(self):
            return BKITParser.RULE_while_stat

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWhile_stat" ):
                return visitor.visitWhile_stat(self)
            else:
                return visitor.visitChildren(self)




    def while_stat(self):

        localctx = BKITParser.While_statContext(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_while_stat)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 382
            self.match(BKITParser.WHILE)
            self.state = 383
            self.match(BKITParser.LP)
            self.state = 386
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,35,self._ctx)
            if la_ == 1:
                self.state = 384
                self.expr()
                pass

            elif la_ == 2:
                self.state = 385
                self.bool_lit()
                pass


            self.state = 388
            self.match(BKITParser.RP)
            self.state = 389
            self.match(BKITParser.DO)
            self.state = 393
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKITParser.VAR) | (1 << BKITParser.IF) | (1 << BKITParser.BREAK) | (1 << BKITParser.WHILE) | (1 << BKITParser.CONITNUE) | (1 << BKITParser.FOR) | (1 << BKITParser.RETURN) | (1 << BKITParser.DO) | (1 << BKITParser.ID))) != 0):
                self.state = 390
                self.statement()
                self.state = 395
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 396
            self.match(BKITParser.ENDWHILE)
            self.state = 397
            self.match(BKITParser.DOT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Do_whileContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DO(self):
            return self.getToken(BKITParser.DO, 0)

        def WHILE(self):
            return self.getToken(BKITParser.WHILE, 0)

        def LP(self):
            return self.getToken(BKITParser.LP, 0)

        def RP(self):
            return self.getToken(BKITParser.RP, 0)

        def ENDDO(self):
            return self.getToken(BKITParser.ENDDO, 0)

        def DOT(self):
            return self.getToken(BKITParser.DOT, 0)

        def expr(self):
            return self.getTypedRuleContext(BKITParser.ExprContext,0)


        def bool_lit(self):
            return self.getTypedRuleContext(BKITParser.Bool_litContext,0)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.StatementContext)
            else:
                return self.getTypedRuleContext(BKITParser.StatementContext,i)


        def getRuleIndex(self):
            return BKITParser.RULE_do_while

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDo_while" ):
                return visitor.visitDo_while(self)
            else:
                return visitor.visitChildren(self)




    def do_while(self):

        localctx = BKITParser.Do_whileContext(self, self._ctx, self.state)
        self.enterRule(localctx, 62, self.RULE_do_while)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 399
            self.match(BKITParser.DO)
            self.state = 403
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,37,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 400
                    self.statement() 
                self.state = 405
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,37,self._ctx)

            self.state = 406
            self.match(BKITParser.WHILE)
            self.state = 407
            self.match(BKITParser.LP)
            self.state = 410
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,38,self._ctx)
            if la_ == 1:
                self.state = 408
                self.expr()
                pass

            elif la_ == 2:
                self.state = 409
                self.bool_lit()
                pass


            self.state = 412
            self.match(BKITParser.RP)
            self.state = 413
            self.match(BKITParser.ENDDO)
            self.state = 414
            self.match(BKITParser.DOT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Break_statContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BREAK(self):
            return self.getToken(BKITParser.BREAK, 0)

        def SEMI(self):
            return self.getToken(BKITParser.SEMI, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_break_stat

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBreak_stat" ):
                return visitor.visitBreak_stat(self)
            else:
                return visitor.visitChildren(self)




    def break_stat(self):

        localctx = BKITParser.Break_statContext(self, self._ctx, self.state)
        self.enterRule(localctx, 64, self.RULE_break_stat)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 416
            self.match(BKITParser.BREAK)
            self.state = 417
            self.match(BKITParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Cont_statContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONITNUE(self):
            return self.getToken(BKITParser.CONITNUE, 0)

        def SEMI(self):
            return self.getToken(BKITParser.SEMI, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_cont_stat

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCont_stat" ):
                return visitor.visitCont_stat(self)
            else:
                return visitor.visitChildren(self)




    def cont_stat(self):

        localctx = BKITParser.Cont_statContext(self, self._ctx, self.state)
        self.enterRule(localctx, 66, self.RULE_cont_stat)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 419
            self.match(BKITParser.CONITNUE)
            self.state = 420
            self.match(BKITParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def call_expr(self):
            return self.getTypedRuleContext(BKITParser.Call_exprContext,0)


        def SEMI(self):
            return self.getToken(BKITParser.SEMI, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_call

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall" ):
                return visitor.visitCall(self)
            else:
                return visitor.visitChildren(self)




    def call(self):

        localctx = BKITParser.CallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 68, self.RULE_call)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 422
            self.call_expr()
            self.state = 423
            self.match(BKITParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ident(self):
            return self.getTypedRuleContext(BKITParser.IdentContext,0)


        def LP(self):
            return self.getToken(BKITParser.LP, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKITParser.ExprContext)
            else:
                return self.getTypedRuleContext(BKITParser.ExprContext,i)


        def RP(self):
            return self.getToken(BKITParser.RP, 0)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(BKITParser.COMMA)
            else:
                return self.getToken(BKITParser.COMMA, i)

        def getRuleIndex(self):
            return BKITParser.RULE_call_expr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_expr" ):
                return visitor.visitCall_expr(self)
            else:
                return visitor.visitChildren(self)




    def call_expr(self):

        localctx = BKITParser.Call_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 70, self.RULE_call_expr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 425
            self.ident()
            self.state = 426
            self.match(BKITParser.LP)
            self.state = 427
            self.expr()
            self.state = 432
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==BKITParser.COMMA:
                self.state = 428
                self.match(BKITParser.COMMA)
                self.state = 429
                self.expr()
                self.state = 434
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 435
            self.match(BKITParser.RP)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Ret_statContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RETURN(self):
            return self.getToken(BKITParser.RETURN, 0)

        def expr(self):
            return self.getTypedRuleContext(BKITParser.ExprContext,0)


        def SEMI(self):
            return self.getToken(BKITParser.SEMI, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_ret_stat

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRet_stat" ):
                return visitor.visitRet_stat(self)
            else:
                return visitor.visitChildren(self)




    def ret_stat(self):

        localctx = BKITParser.Ret_statContext(self, self._ctx, self.state)
        self.enterRule(localctx, 72, self.RULE_ret_stat)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 437
            self.match(BKITParser.RETURN)
            self.state = 438
            self.expr()
            self.state = 439
            self.match(BKITParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Prim_datatypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def int_lit(self):
            return self.getTypedRuleContext(BKITParser.Int_litContext,0)


        def float_lit(self):
            return self.getTypedRuleContext(BKITParser.Float_litContext,0)


        def string_lit(self):
            return self.getTypedRuleContext(BKITParser.String_litContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_prim_datatype

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrim_datatype" ):
                return visitor.visitPrim_datatype(self)
            else:
                return visitor.visitChildren(self)




    def prim_datatype(self):

        localctx = BKITParser.Prim_datatypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 74, self.RULE_prim_datatype)
        try:
            self.state = 444
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKITParser.INTLIT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 441
                self.int_lit()
                pass
            elif token in [BKITParser.FLOATLIT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 442
                self.float_lit()
                pass
            elif token in [BKITParser.STRINGLIT]:
                self.enterOuterAlt(localctx, 3)
                self.state = 443
                self.string_lit()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Int_litContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INTLIT(self):
            return self.getToken(BKITParser.INTLIT, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_int_lit

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInt_lit" ):
                return visitor.visitInt_lit(self)
            else:
                return visitor.visitChildren(self)




    def int_lit(self):

        localctx = BKITParser.Int_litContext(self, self._ctx, self.state)
        self.enterRule(localctx, 76, self.RULE_int_lit)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 446
            self.match(BKITParser.INTLIT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Float_litContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FLOATLIT(self):
            return self.getToken(BKITParser.FLOATLIT, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_float_lit

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFloat_lit" ):
                return visitor.visitFloat_lit(self)
            else:
                return visitor.visitChildren(self)




    def float_lit(self):

        localctx = BKITParser.Float_litContext(self, self._ctx, self.state)
        self.enterRule(localctx, 78, self.RULE_float_lit)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 448
            self.match(BKITParser.FLOATLIT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class String_litContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRINGLIT(self):
            return self.getToken(BKITParser.STRINGLIT, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_string_lit

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString_lit" ):
                return visitor.visitString_lit(self)
            else:
                return visitor.visitChildren(self)




    def string_lit(self):

        localctx = BKITParser.String_litContext(self, self._ctx, self.state)
        self.enterRule(localctx, 80, self.RULE_string_lit)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 450
            self.match(BKITParser.STRINGLIT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Bool_litContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TRUE(self):
            return self.getToken(BKITParser.TRUE, 0)

        def FALSE(self):
            return self.getToken(BKITParser.FALSE, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_bool_lit

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBool_lit" ):
                return visitor.visitBool_lit(self)
            else:
                return visitor.visitChildren(self)




    def bool_lit(self):

        localctx = BKITParser.Bool_litContext(self, self._ctx, self.state)
        self.enterRule(localctx, 82, self.RULE_bool_lit)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 452
            _la = self._input.LA(1)
            if not(_la==BKITParser.TRUE or _la==BKITParser.FALSE):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IdentContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(BKITParser.ID, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_ident

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIdent" ):
                return visitor.visitIdent(self)
            else:
                return visitor.visitChildren(self)




    def ident(self):

        localctx = BKITParser.IdentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 84, self.RULE_ident)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 454
            self.match(BKITParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Mul_divContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT_MUL(self):
            return self.getToken(BKITParser.INT_MUL, 0)

        def FLOAT_MUL(self):
            return self.getToken(BKITParser.FLOAT_MUL, 0)

        def INT_DIV(self):
            return self.getToken(BKITParser.INT_DIV, 0)

        def FLOAT_DIV(self):
            return self.getToken(BKITParser.FLOAT_DIV, 0)

        def MOD(self):
            return self.getToken(BKITParser.MOD, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_mul_div

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMul_div" ):
                return visitor.visitMul_div(self)
            else:
                return visitor.visitChildren(self)




    def mul_div(self):

        localctx = BKITParser.Mul_divContext(self, self._ctx, self.state)
        self.enterRule(localctx, 86, self.RULE_mul_div)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 456
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKITParser.INT_MUL) | (1 << BKITParser.MOD) | (1 << BKITParser.INT_DIV) | (1 << BKITParser.FLOAT_MUL) | (1 << BKITParser.FLOAT_DIV))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Add_subContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT_ADD(self):
            return self.getToken(BKITParser.INT_ADD, 0)

        def FLOAT_ADD(self):
            return self.getToken(BKITParser.FLOAT_ADD, 0)

        def minus(self):
            return self.getTypedRuleContext(BKITParser.MinusContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_add_sub

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAdd_sub" ):
                return visitor.visitAdd_sub(self)
            else:
                return visitor.visitChildren(self)




    def add_sub(self):

        localctx = BKITParser.Add_subContext(self, self._ctx, self.state)
        self.enterRule(localctx, 88, self.RULE_add_sub)
        try:
            self.state = 461
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKITParser.INT_ADD]:
                self.enterOuterAlt(localctx, 1)
                self.state = 458
                self.match(BKITParser.INT_ADD)
                pass
            elif token in [BKITParser.FLOAT_ADD]:
                self.enterOuterAlt(localctx, 2)
                self.state = 459
                self.match(BKITParser.FLOAT_ADD)
                pass
            elif token in [BKITParser.INT_MINUS, BKITParser.FLOAT_MINUS]:
                self.enterOuterAlt(localctx, 3)
                self.state = 460
                self.minus()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MinusContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT_MINUS(self):
            return self.getToken(BKITParser.INT_MINUS, 0)

        def FLOAT_MINUS(self):
            return self.getToken(BKITParser.FLOAT_MINUS, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_minus

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMinus" ):
                return visitor.visitMinus(self)
            else:
                return visitor.visitChildren(self)




    def minus(self):

        localctx = BKITParser.MinusContext(self, self._ctx, self.state)
        self.enterRule(localctx, 90, self.RULE_minus)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 463
            _la = self._input.LA(1)
            if not(_la==BKITParser.INT_MINUS or _la==BKITParser.FLOAT_MINUS):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EqualContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT_EQ(self):
            return self.getToken(BKITParser.INT_EQ, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_equal

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEqual" ):
                return visitor.visitEqual(self)
            else:
                return visitor.visitChildren(self)




    def equal(self):

        localctx = BKITParser.EqualContext(self, self._ctx, self.state)
        self.enterRule(localctx, 92, self.RULE_equal)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 465
            self.match(BKITParser.INT_EQ)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NoteqContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT_NEQ(self):
            return self.getToken(BKITParser.INT_NEQ, 0)

        def FLOAT_NEQ(self):
            return self.getToken(BKITParser.FLOAT_NEQ, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_noteq

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNoteq" ):
                return visitor.visitNoteq(self)
            else:
                return visitor.visitChildren(self)




    def noteq(self):

        localctx = BKITParser.NoteqContext(self, self._ctx, self.state)
        self.enterRule(localctx, 94, self.RULE_noteq)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 467
            _la = self._input.LA(1)
            if not(_la==BKITParser.INT_NEQ or _la==BKITParser.FLOAT_NEQ):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class GreatContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT_GREAT(self):
            return self.getToken(BKITParser.INT_GREAT, 0)

        def FLOAT_GREAT(self):
            return self.getToken(BKITParser.FLOAT_GREAT, 0)

        def INT_GREAT_EQ(self):
            return self.getToken(BKITParser.INT_GREAT_EQ, 0)

        def FLOAT_GREAT_EQ(self):
            return self.getToken(BKITParser.FLOAT_GREAT_EQ, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_great

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGreat" ):
                return visitor.visitGreat(self)
            else:
                return visitor.visitChildren(self)




    def great(self):

        localctx = BKITParser.GreatContext(self, self._ctx, self.state)
        self.enterRule(localctx, 96, self.RULE_great)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 469
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKITParser.INT_GREAT_EQ) | (1 << BKITParser.INT_GREAT) | (1 << BKITParser.FLOAT_GREAT) | (1 << BKITParser.FLOAT_GREAT_EQ))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LessContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT_LESS(self):
            return self.getToken(BKITParser.INT_LESS, 0)

        def FLOAT_LESS(self):
            return self.getToken(BKITParser.FLOAT_LESS, 0)

        def INT_LESS_EQ(self):
            return self.getToken(BKITParser.INT_LESS_EQ, 0)

        def FLOAT_LESS_EQ(self):
            return self.getToken(BKITParser.FLOAT_LESS_EQ, 0)

        def getRuleIndex(self):
            return BKITParser.RULE_less

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLess" ):
                return visitor.visitLess(self)
            else:
                return visitor.visitChildren(self)




    def less(self):

        localctx = BKITParser.LessContext(self, self._ctx, self.state)
        self.enterRule(localctx, 98, self.RULE_less)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 471
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKITParser.INT_LESS_EQ) | (1 << BKITParser.INT_LESS) | (1 << BKITParser.FLOAT_LESS) | (1 << BKITParser.FLOAT_LESS_EQ))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Rel_signContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def equal(self):
            return self.getTypedRuleContext(BKITParser.EqualContext,0)


        def noteq(self):
            return self.getTypedRuleContext(BKITParser.NoteqContext,0)


        def great(self):
            return self.getTypedRuleContext(BKITParser.GreatContext,0)


        def less(self):
            return self.getTypedRuleContext(BKITParser.LessContext,0)


        def getRuleIndex(self):
            return BKITParser.RULE_rel_sign

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRel_sign" ):
                return visitor.visitRel_sign(self)
            else:
                return visitor.visitChildren(self)




    def rel_sign(self):

        localctx = BKITParser.Rel_signContext(self, self._ctx, self.state)
        self.enterRule(localctx, 100, self.RULE_rel_sign)
        try:
            self.state = 477
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKITParser.INT_EQ]:
                self.enterOuterAlt(localctx, 1)
                self.state = 473
                self.equal()
                pass
            elif token in [BKITParser.INT_NEQ, BKITParser.FLOAT_NEQ]:
                self.enterOuterAlt(localctx, 2)
                self.state = 474
                self.noteq()
                pass
            elif token in [BKITParser.INT_GREAT_EQ, BKITParser.INT_GREAT, BKITParser.FLOAT_GREAT, BKITParser.FLOAT_GREAT_EQ]:
                self.enterOuterAlt(localctx, 3)
                self.state = 475
                self.great()
                pass
            elif token in [BKITParser.INT_LESS_EQ, BKITParser.INT_LESS, BKITParser.FLOAT_LESS, BKITParser.FLOAT_LESS_EQ]:
                self.enterOuterAlt(localctx, 4)
                self.state = 476
                self.less()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[13] = self.expr1_sempred
        self._predicates[14] = self.expr2_sempred
        self._predicates[15] = self.expr3_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr1_sempred(self, localctx:Expr1Context, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

    def expr2_sempred(self, localctx:Expr2Context, predIndex:int):
            if predIndex == 1:
                return self.precpred(self._ctx, 2)
         

    def expr3_sempred(self, localctx:Expr3Context, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 2)
         




